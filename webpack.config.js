let path = require('path');
let ExtractTextPlugin = require("extract-text-webpack-plugin");

let conf = {
    entry: './src/index.js', //откуда нужно брать начальный скрипт (это все можно изменять)
    output: { //куда выгружать
        //path: './dist/', // папка куда кладем
        path: path.resolve(__dirname, "./dist"), //куда будет складываться файлы (path.resolve(__dirname нужно устанавливать npm i path --save-dev  смотреть первая строка 1)
        filename: 'main.js', //итоговое назавание файла на выходе (это все можно изменять)
        publicPath: 'dist/' //относительная ссылка на даный файл который будет поставляться из браузера
    },
    devServer: {
        overlay: true //выводить ошибки в консоле (терменале) + в браузере на весь екран
    },
    module: { //в можулях может быть less и все подобное
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                //exclude: '/node_modules/'
            },
            {
                test: /\.css$/,
                //use: [
                  //      'style-loader',
                  //      'css-loader'
                  //  ]
                  use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                  })
                //exclude: '/node_modules/'
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin("styles.css"),
      ]
    //devtool: 'eval-sourcemap'//Строит структуру где можно увидеть режим отладки
    
};

module.exports = conf;